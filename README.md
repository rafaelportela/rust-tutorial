Rust Tutorial
=============

Note: this is a fork from Adrian's original repo with my own solutions (mostly!).
A mirrored copy of his repo is kept here in the `upstream` branch.

We're going to start by working through the first probelm at https://adventofcode.com/2019.

If you want to follow along you have a few choices (in order of what will make you learn the
most):

 1) Install Rust `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`. Run `cargo
 init advent` to start a new project and follow along from there.

 2) Install Rust and clone this repo rather than run `cargo init`.

 3) Don't install anything but paste things into the playground https://play.rust-lang.org/

 4) Just watch 

 All options are totally fine :)

 Note that you will probably want to register at adventofcode.com to get your own input files if
 you're following along. I've also uploaded my own input which you're welcome to use.
