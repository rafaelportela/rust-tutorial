mod day3;
mod day4;

use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let dist = day3::steps_to_shortest_intersection("input-day3.txt");
    println!("day3, part1: distance of closest intersection: {:?}", dist);
    Ok(())
}
