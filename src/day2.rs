use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<dyn Error>> {
    println!("Hello, world!");
    let input = read_to_string("input-day2.txt").expect("Unable to read input");
    let p: Vec<u32> = input
        .split(',')
        .map(|item| item.trim().parse::<u32>().unwrap())
        .collect();

    let target = 19690720;
    let (found, iterations) = search(target, p);
    println!(
        "Found x = 100 * i + j = {} in {} iterations",
        found, iterations
    );

    Ok(())
}

fn search(target: u32, p: Vec<u32>) -> (u32, usize) {
    let mut a = 0;
    let mut b = 10000;
    let mut x = (b - a) / 2;

    let mut iterations = 1;

    while x >= a && x <= b {
        let res = run_program_with_input(x, p.clone());
        if res == target {
            return (x, iterations);
        } else if res < target {
            a = x + 1
        } else {
            b = x - 1;
        }

        x = a + ((b - a) / 2);
        iterations += 1;
    }

    return (0, iterations);
}

fn run_program_with_input(x: u32, p: Vec<u32>) -> u32 {
    let i = x / 100; // quotient gives the line of the matrix
    let j = x % 100; // reminder gives the column of the matrix

    let res = run_program_with_inputs(i, j, p);

    res
}

fn run_program_with_inputs(i: u32, j: u32, mut p: Vec<u32>) -> u32 {
    p[1] = i;
    p[2] = j;

    let result = run_program(p);
    result[0]
}

fn run_program(mut p: Vec<u32>) -> Vec<u32> {
    let mut i = 0 as usize;
    loop {
        match p[i] {
            99 => {
                return p;
            }
            1 => {
                let dest = p[i + 3] as usize;
                let a = p[i + 1] as usize;
                let b = p[i + 2] as usize;
                p[dest] = p[a] + p[b];
            }
            2 => {
                let dest = p[i + 3] as usize;
                let a = p[i + 1] as usize;
                let b = p[i + 2] as usize;
                p[dest] = p[a] * p[b];
            }
            _ => {
                unreachable!();
            }
        }
        i += 4;
    }
}

#[cfg(test)]
mod tests {
    use super::run_program;
    use super::run_program_with_input;
    use super::run_program_with_inputs;
    use std::fs::read_to_string;

    #[test]
    fn testprogram() {
        assert_eq!(run_program(vec![1, 0, 0, 0, 99]), vec![2, 0, 0, 0, 99]);
        assert_eq!(run_program(vec![2, 3, 0, 3, 99]), vec![2, 3, 0, 6, 99]);
        assert_eq!(
            run_program(vec![1, 1, 1, 4, 99, 5, 6, 0, 99]),
            vec![30, 1, 1, 4, 2, 5, 6, 0, 99]
        );

        // the original input should be used, otherwise the program reaches "unreachable!"
        let input = read_to_string("input-day2.txt").expect("Unable to read input");
        let p: Vec<u32> = input
            .split(',')
            .map(|item| item.trim().parse::<u32>().unwrap())
            .collect();

        assert_eq!(
            run_program_with_input(223, p.clone()),
            run_program_with_inputs(2, 23, p.clone())
        );
    }
}
