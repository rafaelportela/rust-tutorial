use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<dyn Error>> {
    println!("Hello, world!");
    let input = read_to_string("input.txt").expect("Unable to read input");
    let input: Vec<_> = input.lines().map(|line| line.parse::<usize>()).collect();

    let mut total_fuel = 0;
    for i in input {
        total_fuel += calculate_fuel(i.unwrap_or(0));
    }

    println!("{:?}", total_fuel);
    Ok(())
}

fn calculate_fuel(mass: usize) -> usize {
    let fuel = (mass / 3) - 2;
    fuel
}

#[cfg(test)]
mod tests {
    use super::calculate_fuel;

    #[test]
    fn testfuel() {
        assert_eq!(calculate_fuel(12), 2);
        assert_eq!(calculate_fuel(14), 2);
        assert_eq!(calculate_fuel(1968), 654);
        assert_eq!(calculate_fuel(100756), 33583);
    }
}
