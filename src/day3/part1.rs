use super::point;
use super::Point;
use std::collections::HashSet;

pub fn visited_points(wire: Vec<&str>) -> HashSet<Point> {
    let mut visited = HashSet::<Point>::new();
    let mut origin = Point { x: 0, y: 0 };
    for piece in wire {
        let segment = segment(&origin, piece);
        for pt in &segment {
            visited.insert(*pt);
        }
        origin = segment.last().unwrap().clone();
    }
    visited
}

pub fn segment(p: &Point, vector: &str) -> Vec<Point> {
    let (direction, mag) = vector.split_at(1);
    let mag = mag.parse::<isize>().unwrap();

    match direction {
        "R" => (0..mag).map(|step| point(p.x + step + 1, p.y)).collect(),
        "L" => (0..mag).map(|step| point(p.x - step - 1, p.y)).collect(),
        "U" => (0..mag).map(|step| point(p.x, p.y + step + 1)).collect(),
        "D" => (0..mag).map(|step| point(p.x, p.y - step - 1)).collect(),
        _ => panic!("Shouldn't happen!"),
    }
}

pub fn manhattan_distance(p1: Point, p2: Point) -> isize {
    (p1.x - p2.x).abs() + (p1.y - p2.y).abs()
}

#[cfg(test)]
mod tests {
    use super::manhattan_distance;
    use super::point;
    use super::segment;
    use super::visited_points;
    use super::Point;

    #[test]
    fn segments() {
        let o = &point(0, 0);
        assert_eq!(segment(o, "R2"), vec![point(1, 0), point(2, 0)]);
        assert_eq!(segment(o, "L2"), vec![point(-1, 0), point(-2, 0)]);
        assert_eq!(segment(o, "U2"), vec![point(0, 1), point(0, 2)]);
        assert_eq!(segment(o, "D2"), vec![point(0, -1), point(0, -2)]);
        let p = &point(2, 2);
        assert_eq!(
            segment(p, "R3"),
            vec![point(3, 2), point(4, 2), point(5, 2)]
        );
    }

    #[test]
    fn set_of_visited_points() {
        // the example provided in the problem description
        assert_eq!(visited_points(vec!["R2", "U2"]).len(), 4);
        let visited1 = visited_points(vec!["R8", "U5", "L5", "D3"]);
        let visited2 = visited_points(vec!["U7", "R6", "D4", "L4"]);
        assert_eq!(visited1.len(), 8 + 5 + 5 + 3);
        assert_eq!(visited2.len(), 7 + 6 + 4 + 4);
        let intersection: Vec<&Point> = visited1.intersection(&visited2).collect();

        // why && is needed?
        assert!(intersection.contains(&&Point { x: 3, y: 3 }));
    }

    #[test]
    fn manhattan_distances() {
        assert_eq!(manhattan_distance(point(1, 1), point(2, 3)), 3);
    }
}
