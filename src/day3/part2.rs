use super::part1;
use super::Point;
use std::collections::HashMap;

pub fn steps_to_shortest_intersection(wire1: Vec<&str>, wire2: Vec<&str>) -> Option<i32> {
    let mut steps_count_1 = 0;
    let pts1 = visit(&wire1).map(|point| {
        steps_count_1 += 1;
        VisitedPoint {
            point,
            steps: steps_count_1,
        }
    });

    let mut steps_count_2 = 0;
    let pts2 = visit(&wire2).map(|point| {
        steps_count_2 += 1;
        VisitedPoint {
            point,
            steps: steps_count_2,
        }
    });

    let mut all_visited_1 = HashMap::<Point, i32>::new();
    let mut all_visited_2 = HashMap::<Point, i32>::new();
    for (visited1, visited2) in pts1.zip(pts2) {
        // wire1's current point crosses wire2?
        match all_visited_2.get(&visited1.point) {
            // yes: return sum of steps
            Some(steps) => return Some(visited1.steps + steps),
            _ => {
                // no: insert visited1 in set 1
                all_visited_1
                    .entry(visited1.point)
                    .or_insert(visited1.steps);
            }
        }

        // wire2's current point crosses wire1?
        match all_visited_1.get(&visited2.point) {
            Some(steps) => return Some(visited2.steps + steps),
            _ => {
                all_visited_2
                    .entry(visited2.point)
                    .or_insert(visited2.steps);
            }
        }
    }

    None // no intersection found
}

struct VisitedPoint {
    point: Point,
    steps: i32,
}

fn visit<'a>(wire: &'a Vec<&'a str>) -> impl Iterator<Item = Point> + 'a {
    let mut last_point = Point { x: 0, y: 0 };

    // use the 'move' keyword to force the closure to take ownership of 'last_point'. Otherwise,
    // 'turn' may outlive the borrowed value 'last_point'.
    wire.iter().flat_map(move |turn| {
        let points = part1::segment(&last_point, turn);
        last_point = points.last().unwrap().clone();
        points
    })
}

#[cfg(test)]
mod tests {
    use super::steps_to_shortest_intersection;
    #[test]
    fn example1() {
        let w1 = vec!["R8", "U5", "L5", "D3"];
        let w2 = vec!["U7", "R6", "D4", "L4"];
        let steps = steps_to_shortest_intersection(w1, w2);
        assert_eq!(steps, Some(30));
    }

    #[test]
    #[ignore]
    fn example2() {
        let w1 = vec!["R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"];
        let w2 = vec!["U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"];
        let steps = steps_to_shortest_intersection(w1, w2);
        // description of this example presents wrong result?
        assert_eq!(steps, Some(610));
    }

    #[test]
    fn example3() {
        let w1 = vec![
            "R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51",
        ];
        let w2 = vec![
            "U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7",
        ];
        let steps = steps_to_shortest_intersection(w1, w2);
        assert_eq!(steps, Some(410));
    }
}
