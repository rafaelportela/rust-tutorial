pub mod part1;
pub mod part2;

use part1::manhattan_distance;
use part1::visited_points;
use std::fs::read_to_string;

pub fn distance_to_closest_intersection(input_file: &str) -> isize {
    let input = read_to_string(input_file).expect("Unable to read input");
    let mut lines = input.lines();
    let first_line = lines.next().unwrap();
    let second_line = lines.next().unwrap();

    let wire1: Vec<&str> = first_line.split(',').collect();
    let wire2: Vec<&str> = second_line.split(',').collect();

    let visited1 = visited_points(wire1);
    let visited2 = visited_points(wire2);

    let intersection = visited1.intersection(&visited2);

    let closest_intersection = intersection
        .map(|pt| manhattan_distance(point(0, 0), *pt))
        .min()
        .unwrap();

    closest_intersection
}

pub fn steps_to_shortest_intersection(input_file: &str) -> Option<i32> {
    let input = read_to_string(input_file).expect("Unable to read input");
    let mut lines = input.lines();
    let first_line = lines.next().unwrap();
    let second_line = lines.next().unwrap();

    let wire1: Vec<&str> = first_line.split(',').collect();
    let wire2: Vec<&str> = second_line.split(',').collect();

    part2::steps_to_shortest_intersection(wire1, wire2)
}

#[derive(Eq, PartialEq, Hash, Clone, Copy, Debug)]
pub struct Point {
    pub x: isize,
    pub y: isize,
}

// Builder to construct a Point like `point(2,3)`.
pub fn point(x: isize, y: isize) -> Point {
    Point { x, y }
}
