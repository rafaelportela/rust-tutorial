use std::collections::HashMap;

pub fn num_of_possible_passwords_part1(begin: i32, end: i32) -> i32 {
    let mut curr = begin;
    let mut passwords = 0;
    while curr < end {
        if is_possible_password_part1(&curr) {
            passwords += 1;
        }
        curr += 1;
    }

    passwords
}

pub fn num_of_possible_passwords_part2(begin: i32, end: i32) -> i32 {
    let mut curr = begin;
    let mut passwords = 0;
    while curr < end {
        if is_possible_password_part2(&curr) {
            passwords += 1;
        }
        curr += 1;
    }

    passwords
}

fn is_possible_password_part1(num: &i32) -> bool {
    let vec = as_vec(num);
    has_similar_adjacent_digits(&vec) && has_increasing_or_same_digits(&vec)
}

fn is_possible_password_part2(num: &i32) -> bool {
    let vec = as_vec(num);

    has_similar_adjacent_digits_not_part_of_larger_group(&vec)
        && has_increasing_or_same_digits(&vec)
}

fn has_similar_adjacent_digits(vec: &Vec<u32>) -> bool {
    for i in 0..vec.len() - 1 {
        if vec[i] == vec[i + 1] {
            return true;
        }
    }
    false
}

fn has_increasing_or_same_digits(digits: &Vec<u32>) -> bool {
    for i in 0..digits.len() - 1 {
        if digits[i] > digits[i + 1] {
            return false;
        }
    }
    true
}

fn has_similar_adjacent_digits_not_part_of_larger_group(digits: &Vec<u32>) -> bool {
    let mut adj = HashMap::new();

    // count how many times each digit appears
    for d in digits {
        // considering digits always increase, repeating digitis will
        // always be adjancent
        let count = adj.entry(d).or_insert(0);
        *count += 1;
    }

    for val in adj.values() {
        // there's one adjacent pair which is not part of a larger group
        if val == &2 {
            return true;
        }
    }

    false
}

fn as_vec(num: &i32) -> Vec<u32> {
    num.to_string()
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .collect()
}

#[cfg(test)]
mod tests {
    use super::as_vec;
    use super::has_increasing_or_same_digits;
    use super::has_similar_adjacent_digits_not_part_of_larger_group;
    use super::is_possible_password_part1;
    use super::is_possible_password_part2;
    use super::num_of_possible_passwords_part1;
    use super::num_of_possible_passwords_part2;

    #[test]
    fn count_passwords() {
        // This is the real question and my provided inputs (and right answer!):
        // How many different passwords within the range given in your puzzle input meet these
        // criteria?
        assert_eq!(num_of_possible_passwords_part1(231832, 767346), 1330);
    }

    #[test]
    fn count_passwords_part2() {
        assert_eq!(num_of_possible_passwords_part2(231832, 767346), 876);
    }

    #[test]
    fn test_password_rules() {
        assert!(is_possible_password_part1(&111111)); // double 11, never decreases
        assert!(!is_possible_password_part1(&223450)); // decreasing pair of digits 50
        assert!(!is_possible_password_part1(&123789)); // no double
    }

    #[test]
    fn test_increasing_digits_rule() {
        assert!(has_increasing_or_same_digits(&vec![1, 1, 1, 1, 2, 3]));
        assert!(has_increasing_or_same_digits(&vec![1, 3, 5, 6, 7, 9]));
        assert!(!has_increasing_or_same_digits(&vec![2, 1]));
    }

    #[test]
    fn test_adjacent_digits_not_part_of_larger_group_rule() {
        assert!(has_similar_adjacent_digits_not_part_of_larger_group(&vec![
            1, 1, 2, 2, 3, 3
        ]));
        assert!(!has_similar_adjacent_digits_not_part_of_larger_group(
            &vec![1, 2, 3, 4, 4, 4]
        ));
        assert!(has_similar_adjacent_digits_not_part_of_larger_group(&vec![
            1, 1, 1, 1, 2, 2
        ]));
        assert!(!has_similar_adjacent_digits_not_part_of_larger_group(
            &vec![1, 1, 1, 1, 2, 3]
        ));
    }

    #[test]
    fn test_as_vec() {
        assert_eq!(as_vec(&123), vec![1, 2, 3]);
    }
}
